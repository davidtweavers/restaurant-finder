import React, {Component} from 'react';
import axios from 'axios';
import './App.css';

import People from './components/People/People';
import PlacesToGo from './components/PlacesToGo/PlacesToGo';
import PlacesToAvoid from './components/PlacesToAvoid/PlacesToAvoid';


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      people:[],
      locations:[],
      current_people:[],
      places_to_go:[],
      place_to_skip:[],
      places_people_skip_food:[],
      places_people_skip_drink: [],
      loaded: false
    }
  }

  componentDidMount () {
    axios.get('https://gist.githubusercontent.com/benjambles/ea36b76bc5d8ff09a51def54f6ebd0cb/raw/524e40ec297353b8070ff10ee0d9d847e44210f5/users.json')
    .then(res => {

      const people = res.data.map((item,index) => {
        return {
          name: item.name,
          wont_eat: this.jsonCleanUp(item.wont_eat),
          drinks: this.jsonCleanUp(item.drinks),
          id: index + 1
        };
      })

      this.setState({ 
        people
      })

    }).catch(err => {
      console.log(err)
    })

    axios.get('https://gist.githubusercontent.com/benjambles/ea36b76bc5d8ff09a51def54f6ebd0cb/raw/524e40ec297353b8070ff10ee0d9d847e44210f5/venues.json')
    .then(res => {
      let regex = /,(?=\s*?[}\]])/g;
      let correct = res.data.replace(regex, ''); // remove all trailing commas
      let data = JSON.parse(correct);

      const locations = data.map((item,index) => {
        return {
          name: item.name,
          food: this.jsonCleanUp(item.food),
          drinks: this.jsonCleanUp(item.drinks),
          id: index + 1
        };
      })

      this.setState({ 
        locations,
        places_to_go: locations,
        loaded: true
      })
      
    }).catch(err => {
      console.log(err)
    })
  }

  onChangeUpdatePeople = (event,id) => {

    let peopleArr = [...this.state.current_people];

    if(!event.target.checked){
      peopleArr.splice(peopleArr.indexOf(id),1)
    }else{
      peopleArr.push(id)
    }

    this.setState({
      current_people: peopleArr
    }, this.updateRecommendations )

  }

  updateRecommendations = () => {

    let peopleCopy = [...this.state.people];

    let locationCopy = [...this.state.locations];

    let hydratePeople = [];

    for (let arrItem of this.state.current_people){
      hydratePeople.push(peopleCopy.find(item => {
        return item.id === arrItem
        }))
    }

    let badFood = [];
    let badDrink = [];
    let avoidFoodPeopleArr = [];
    let avoidDrinkPeopleArr = [];

    for (let objItem of hydratePeople){
      let canDrink = [];
      for (let foodItem of objItem.wont_eat){
        let avoidPlaces = locationCopy.filter(item => {
          return item.food.some(function(item){
            return item === foodItem;
          });
        })
        
        let avoidFoodPeople = avoidPlaces.map(obj=> (
            {
              [obj.id] : {
                  name : objItem.name
              }
            }
        ))

        avoidFoodPeopleArr.push(avoidFoodPeople)
        badFood.push(avoidPlaces)
      }
      for (let drinkItem of objItem.drinks){
        canDrink.push(locationCopy.filter(item => {
          return item.drinks.find(function(item){
            return item === drinkItem;
          });
        })
        )
      }
      
      //we found the places that had the drink now need to work out the places they can't drink

      let tempArr = this.arrCleanUp(canDrink)

      tempArr = this.findMissingObjects(locationCopy,tempArr)
      badDrink.push(tempArr)

      let avoidDrinkPeople = tempArr.map(obj=> (
        {
          [obj.id] : {
              name : objItem.name
          }
        }
      ))
      avoidDrinkPeopleArr.push(avoidDrinkPeople)
    }

      badFood = this.arrCleanUp(badFood)

      let allBad = [...new Set(badFood.concat(...badDrink))]

      let allGood = this.findMissingObjects(locationCopy,allBad)

      avoidFoodPeopleArr = this.arrCleanUp(avoidFoodPeopleArr)

      avoidFoodPeopleArr = this.getUniqArray(avoidFoodPeopleArr)

      avoidDrinkPeopleArr = this.arrCleanUp(avoidDrinkPeopleArr)

    this.setState({
      places_to_go : allGood,
      place_to_skip : allBad,
      places_people_skip_food: avoidFoodPeopleArr,
      places_people_skip_drink: avoidDrinkPeopleArr
    })

  }

  findMissingObjects = (arr1,arr2) => {

    return arr1.filter(function(obj) {
        return !arr2.some(function(obj2) {
            return obj.name === obj2.name;
        });
    });

  }

  arrCleanUp = (arr) => {
    let newArr = arr.reduce(
      (arr, elem) => [...arr, ...elem], []
    )

    return [...new Set(newArr)];
  }
  
  getUniqArray = (arr) => {

    const uniq = new Set(arr.map(e => JSON.stringify(e)));
      
    return Array.from(uniq).map(e => JSON.parse(e))

  }

  jsonCleanUp = (arr) => {
      const newArr = arr.map((item)=>{
        if(item === "Vokda"){item = "Vodka"}
        return item.toLowerCase();
      })

      return newArr;
  }

  render(){
    return (
      <div className="App">
        <People people={this.state.people} updatePeople = {this.onChangeUpdatePeople}/>
        <PlacesToGo list={this.state.places_to_go} loaded={this.state.loaded}/>
        <PlacesToAvoid list={this.state.place_to_skip} drinkSkip={this.state.places_people_skip_drink} foodSkip={this.state.places_people_skip_food}/>
      </div>
    );
  }

  
}

export default App;
