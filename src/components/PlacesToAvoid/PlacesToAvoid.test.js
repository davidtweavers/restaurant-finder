import React from 'react';
import { shallow , mount } from 'enzyme';
import PlacesToAvoid from './PlacesToAvoid';


describe('<PlacesToAvoid />', () => {

  it('renders without crashing', () => {
    shallow(<PlacesToAvoid list={[
      {name: "Twin Dynasty", food: Array(1), drinks: Array(5), id: 2},
      {name: "Fabrique", food: Array(3), drinks: Array(3), id: 9}
  ]}/>);
  });

  it('renders places and person info', () => {
    let wrapper;
    wrapper = shallow(<PlacesToAvoid list={[
      {name: "Twin Dynasty", food: Array(1), drinks: Array(5), id: 2},
      {name: "Fabrique", food: Array(3), drinks: Array(3), id: 9}
  ]} foodSkip={[{2:{name: "David Lang"}}]} drinkSkip={[{9:{name: "David Lang"}}]}/>);
    expect(wrapper.find('li')).toHaveLength(4);
  });

})