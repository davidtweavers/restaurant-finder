import React from 'react';

const placesToAvoid = (props) => {
  return (
    <div>
      <p>Places To Avoid</p>
      <ul>
      {props.list.map((item) => {
        return (
          <li key={item.id}>
            {item.name}
            <ul>
            {props.foodSkip && props.foodSkip.filter((restItem,index) => {
              return parseInt(Object.keys(restItem)[0]) === item.id
            }).map((filterItem,index) => {
              return <li key={index}>{filterItem[item.id].name} for the food</li>
            })}
            {props.drinkSkip && props.drinkSkip.filter((restItem,index) => {
              return parseInt(Object.keys(restItem)[0]) === item.id
            }).map((filterItem,index) => {
              return <li key={index}>{filterItem[item.id].name} for the lack of drink choice</li>
            })}
            </ul>
          </li>
        )
      })}
      </ul>
    </div>
  )
}

export default placesToAvoid;