import React from 'react';

const placesToGo = (props) => {
  return (
    <div>
      <p>Places To Go</p>
      {props.list.length ? 
      <ul>
        {props.list.map(item => {
          return <li key={item.id}>{item.name}</li>
        })}
      </ul>
      :
      props.loaded ? <strong>No matches</strong> : null}
    </div>
    
  )
}

export default placesToGo;