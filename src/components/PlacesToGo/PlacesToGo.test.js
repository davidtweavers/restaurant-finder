import React from 'react';
import { shallow , mount } from 'enzyme';
import PlacesToGo from './PlacesToGo';


describe('<PlacesToGo /> rendering', () => {

  it('renders without crashing', () => {
    shallow(<PlacesToGo list={[
      {name: "Twin Dynasty", food: Array(1), drinks: Array(5), id: 2},
      {name: "Fabrique", food: Array(3), drinks: Array(3), id: 9}
  ]}/>);
  });

})