import React from 'react';
import { shallow , mount } from 'enzyme';
import People from './People';


describe('<People /> rendering', () => {

  it('renders without crashing', () => {
    shallow(<People people={[
      {
        "name": "John Davis",
        "wont_eat": [
          "fish"
        ],
        "drinks": [
          "cider",
          "rum",
          "soft drinks"
        ],
        "id": 1
    }]}/>);
  });

})