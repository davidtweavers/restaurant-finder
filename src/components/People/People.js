import React from 'react';

const people = (props) => {
  return (
    <div>
      <p>Staff</p>
      <form>
        {props.people.map((item)=>{
        return (
          <span key={item.id}>
            <input 
            type="checkbox" 
            id={item.name} 
            value={item.id} 
            name={item.name} 
            onChange={(event) => props.updatePeople(event,item.id)}/>
            <label htmlFor={item.name}>
              {item.name}
            </label>
          </span>
        )
        })}
      </form>

    </div>
  )
}

export default people;